package com.dinfogarneau.coursmobile.exercices.tp3.ui.favoris;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp3.R;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FavorisListeAdapter extends RecyclerView.Adapter<FavorisListeAdapter.FavorisViewHolder> {
    private List<Propriete> proprieteList;

    public FavorisListeAdapter(ArrayList<Propriete> proprietes) {
        this.proprieteList = proprietes;
    }

    private FavorisListeAdapter.onItemClickListener mListener;

    public void setProprieteList(List<Propriete> proprietes) { proprieteList = proprietes; }

    public List<Propriete> getProprieteList() { return proprieteList; }

    public interface onItemClickListener {
        void naviguerDetails(Propriete propriete);
    }

    public void setOnClickListener(FavorisListeAdapter.onItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public FavorisListeAdapter.FavorisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.proprietes_item, parent, false);
        return new FavorisViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavorisListeAdapter.FavorisViewHolder viewHolder, int position) {
        if (proprieteList != null) {
            Propriete current = proprieteList.get(position);
            viewHolder.tvType.setText(current.getType());
            viewHolder.tvPrix.setText(current.getPrix());
            viewHolder.tvAdresse.setText(current.getAdresse());
            viewHolder.tvVille.setText(current.getVille());
            Picasso.get().load(current.getPetitePhoto())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder).into(viewHolder.ivPropriete);
        }
    }

    @Override
    public int getItemCount() {
        if (proprieteList != null)
            return proprieteList.size();
        else return 0;
    }

    public class FavorisViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType;
        public TextView tvPrix;
        public TextView tvAdresse;
        public TextView tvVille;
        public ImageView ivPropriete;

        public FavorisViewHolder(@NonNull View itemView) {
            super(itemView);
            tvType = itemView.findViewById(R.id.tv_details_type);
            tvPrix = itemView.findViewById(R.id.tv_prix);
            tvAdresse = itemView.findViewById(R.id.tv_adresse);
            tvVille = itemView.findViewById(R.id.tv_city);
            ivPropriete = itemView.findViewById(R.id.iv_propriete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        // Bonne pratique pour s'assurer que l'élément clické est toujours présent
                        if (position != RecyclerView.NO_POSITION) {
                            Propriete propriete = proprieteList.get(position);
                            mListener.naviguerDetails(propriete);
                        }
                    }
                }
            });
        }
    }
}
