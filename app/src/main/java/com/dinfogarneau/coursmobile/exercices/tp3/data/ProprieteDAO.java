package com.dinfogarneau.coursmobile.exercices.tp3.data;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dinfogarneau.coursmobile.exercices.tp3.model.Commentaire;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ProprieteDAO {

    @Insert
    void insert(Propriete propriete);

    @Insert
    void insert(Commentaire commentaire);

    @Update
    void updatePropriete(Propriete... proprietes);

    @Update
    void updateCommentaire(Commentaire... commentaire);

    @Delete
    void delete(Propriete propriete);

    @Delete
    void delete(Commentaire commentaire);

    @Query("DELETE FROM propriete")
    void deleteAllPropriete();

    @Query("DELETE FROM commentaire")
    void deleteAllCommentaire();

    @Query("SELECT commentaire FROM commentaire WHERE propriete_id = :proprieteId ")
    String getCommentaireByPropId(int proprieteId);

    @Query("SELECT * FROM propriete")
    List<Propriete> getAllProprietes();

    @Query("SELECT * FROM propriete ORDER BY prix ASC")
    LiveData<List<Propriete>> getAllLiveProprietes();

    @Query("SELECT * FROM propriete WHERE favoris = 1 ORDER BY prix ASC")
    LiveData<List<Propriete>> getAllLiveFavoris();
}
