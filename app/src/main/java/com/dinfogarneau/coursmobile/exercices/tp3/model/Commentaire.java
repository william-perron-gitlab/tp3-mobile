package com.dinfogarneau.coursmobile.exercices.tp3.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "commentaire",
        foreignKeys = {@ForeignKey(entity = Propriete.class,
        parentColumns = "id",
        childColumns = "propriete_id",
        onDelete = ForeignKey.CASCADE)})
public class Commentaire {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name="propriete_id")
    private int proprieteId;

    @NonNull
    @ColumnInfo(name="commentaire")
    private String commentaire;

    public Commentaire(int proprieteId, @NonNull String commentaire) {
        this.proprieteId = proprieteId;
        this.commentaire = commentaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProprieteId() {
        return proprieteId;
    }

    public void setProprieteId(int proprieteId) {
        this.proprieteId = proprieteId;
    }

    @NonNull
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(@NonNull String commentaire) {
        this.commentaire = commentaire;
    }
}
