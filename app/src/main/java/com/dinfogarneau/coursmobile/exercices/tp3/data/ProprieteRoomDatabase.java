package com.dinfogarneau.coursmobile.exercices.tp3.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dinfogarneau.coursmobile.exercices.tp3.model.Commentaire;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

@Database(entities = {Propriete.class, Commentaire.class}, version = 1)
public abstract class ProprieteRoomDatabase extends RoomDatabase {

    public static volatile ProprieteRoomDatabase INSTANCE;

    public abstract ProprieteDAO proprieteDAO();

    public static synchronized ProprieteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            // Crée la BDD
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    ProprieteRoomDatabase.class, "crypto_database")
                    .build();
        }
        return INSTANCE;
    }
}
