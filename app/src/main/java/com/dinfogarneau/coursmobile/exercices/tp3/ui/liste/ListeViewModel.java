package com.dinfogarneau.coursmobile.exercices.tp3.ui.liste;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

import java.util.ArrayList;
import java.util.List;

public class ListeViewModel extends AndroidViewModel {

    private LiveData<List<Propriete>> proprietes;
    private ProprieteRoomDatabase mdb;

    public ListeViewModel(@NonNull Application application) {
        super(application);
        mdb = ProprieteRoomDatabase.getDatabase(application);
        proprietes = mdb.proprieteDAO().getAllLiveProprietes();

    }

    public LiveData<List<Propriete>> getProprietes() {
        return proprietes;
    }
}