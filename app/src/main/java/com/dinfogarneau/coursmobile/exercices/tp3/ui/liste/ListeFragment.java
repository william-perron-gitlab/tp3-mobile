package com.dinfogarneau.coursmobile.exercices.tp3.ui.liste;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDeepLink;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.DialogFragmentNavigator;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp3.R;
import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.datahttp.VolleyUtils;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

import java.util.ArrayList;
import java.util.List;

public class ListeFragment extends Fragment {

    private ProprieteRoomDatabase mDb;
    private ListeViewModel listeViewModel;
    private RecyclerView rvProprietes;
    private ArrayList<Propriete> proprietes;
    private ListeAdapter listeAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        listeViewModel = new ViewModelProvider(requireActivity()).get(ListeViewModel.class);

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = ProprieteRoomDatabase.getDatabase(getActivity());

        rvProprietes = view.findViewById(R.id.rv_proprietes);
        proprietes = new ArrayList<>();
        listeAdapter = new ListeAdapter(proprietes);
        rvProprietes.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvProprietes.setAdapter(listeAdapter);

        listeAdapter.setOnClickListener(new ListeAdapter.onItemClickListener() {
            @Override
            public void ajouterFavoris(Propriete propriete) {
                propriete.setFavoris(true);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDAO().updatePropriete(propriete);
                    }
                });
            }

            @Override
            public void naviguerDetails(Propriete propriete) {
                NavDirections action = ListeFragmentDirections.actionNavListeToNavDetails(propriete);
                Navigation.findNavController(view).navigate(action);
            }
        });

        if (listeViewModel.getProprietes() != null) {
        listeViewModel.getProprietes().observe(getViewLifecycleOwner(), new Observer<List<Propriete>>() {
            @Override
            public void onChanged(List<Propriete> proprietes) {
                listeAdapter.setProprieteList(proprietes);
                listeAdapter.notifyDataSetChanged();
            }
        });
        }

    }
}