package com.dinfogarneau.coursmobile.exercices.tp3.datahttp;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyUtils {

    private static final String TAG = "tag";
    private ArrayList<Propriete> proprietes = new ArrayList<>();

    public interface ListProprietesAsyncResponse {
        void processFinished(ArrayList<Propriete> proprieteArrayList);
    }

    public void getProprietes(Context context, ListProprietesAsyncResponse callback) {
        String url = "https://onoup.site/featured-homes.json";

        // Request a string response from the provided URL.
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    Propriete propriete = new Propriete();
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        JSONObject photos = jsonObject.getJSONObject("photos");
                        propriete.setFavoris(false);
                        propriete.setAdresse(jsonObject.getString("street"));
                        propriete.setVille(jsonObject.getString("city"));
                        propriete.setId(jsonObject.getInt("id"));
                        propriete.setLatitude(jsonObject.getString("latitude"));
                        propriete.setLongitude(jsonObject.getString("longitude"));
                        propriete.setPrix(jsonObject.getString("price_display"));
                        propriete.setType(jsonObject.getString("type"));
                        propriete.setPetitePhoto(photos.getString("_155"));
                        propriete.setGrandePhoto(photos.getString("_1600"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    proprietes.add(propriete);

                }

                if (callback != null) callback.processFinished(proprietes);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error);
            }
        });

        // Add the request to the RequestQueue.
        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);

    }
}
