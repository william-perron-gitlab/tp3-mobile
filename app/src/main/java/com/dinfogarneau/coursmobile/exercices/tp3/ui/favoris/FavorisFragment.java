package com.dinfogarneau.coursmobile.exercices.tp3.ui.favoris;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp3.R;
import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;
import com.dinfogarneau.coursmobile.exercices.tp3.ui.liste.ListeAdapter;
import com.dinfogarneau.coursmobile.exercices.tp3.ui.liste.ListeFragmentDirections;
import com.dinfogarneau.coursmobile.exercices.tp3.ui.liste.ListeViewModel;

import java.util.ArrayList;
import java.util.List;

public class FavorisFragment extends Fragment {

    private FavorisViewModel favorisViewModel;
    private ProprieteRoomDatabase mDb;
    private RecyclerView rvProprietes;
    private ArrayList<Propriete> proprietes;
    private FavorisListeAdapter favorisAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        favorisViewModel = new ViewModelProvider(this).get(FavorisViewModel.class);
        return inflater.inflate(R.layout.fragment_favoris, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = ProprieteRoomDatabase.getDatabase(getActivity());

        rvProprietes = view.findViewById(R.id.rv_proprietes);
        proprietes = new ArrayList<>();
        favorisAdapter = new FavorisListeAdapter(proprietes);
        rvProprietes.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvProprietes.setAdapter(favorisAdapter);

        favorisAdapter.setOnClickListener(new FavorisListeAdapter.onItemClickListener() {
            @Override
            public void naviguerDetails(Propriete propriete) {
                NavDirections action = FavorisFragmentDirections.actionNavFavorisToNavDetails(propriete);
                Navigation.findNavController(view).navigate(action);
            }
        });

        favorisViewModel.getFavoris().observe(getViewLifecycleOwner(), new Observer<List<Propriete>>() {
            @Override
            public void onChanged(List<Propriete> proprietes) {
                favorisAdapter.setProprieteList(proprietes);
                favorisAdapter.notifyDataSetChanged();
            }
        });
    }
}