package com.dinfogarneau.coursmobile.exercices.tp3.ui.details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.room.ColumnInfo;

import com.dinfogarneau.coursmobile.exercices.tp3.model.Commentaire;
import com.dinfogarneau.coursmobile.exercices.tp3.R;

public class DialogCommFragment extends DialogFragment {

    private static final String MODIFIER_TITLE = "Modifier le commentaire";
    private static final String AJOUTER_TITLE = "Ajouter un commentaire";

    private static final String MODIFIER = "Modifier";
    private static final String AJOUTER = "Ajouter";
    private static final String ANNULER = "Annuler";
    private static final String ERREUR = "Le commentaire ne doit pas être vide";

    private String commentaire;
    private int propriete_id;

    public interface EditCommentaireDialogListener {
        void onFinishEditDialog(Commentaire commentaire);
    }

    public DialogCommFragment(int propriete_id) {
        this.propriete_id = propriete_id;
    }

    public DialogCommFragment(String commentaire, int propriete_id) {
        this.commentaire = commentaire;
        this.propriete_id = propriete_id;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String formButton = "";
        if (this.commentaire != null) {
            formButton = MODIFIER;
        } else {
            formButton = AJOUTER;
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.commentaire_form, null);
        final Context context = getContext();

        if (commentaire != null) {
            EditText et_commentaire = v.findViewById(R.id.et_commentaire);
            et_commentaire.setText(commentaire);
        }

        builder.setView(v)
                .setPositiveButton(formButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText et_commentaire = v.findViewById(R.id.et_commentaire);



                        String commentaire_trim = et_commentaire.getText().toString().trim();

                        if (commentaire_trim.isEmpty()) {
                            Toast.makeText(context, ERREUR, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Commentaire commentaireObj = new Commentaire(propriete_id, commentaire_trim);

                        EditCommentaireDialogListener listener = (EditCommentaireDialogListener) getActivity();
                        listener.onFinishEditDialog(commentaireObj);
                    }

                })
                .setNegativeButton(ANNULER, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        // Create the AlertDialog object and return it
        if (commentaire != null) {
            builder.setTitle(MODIFIER_TITLE);
        } else {
            builder.setTitle(AJOUTER_TITLE);
        }
        return builder.create();
    }
}
