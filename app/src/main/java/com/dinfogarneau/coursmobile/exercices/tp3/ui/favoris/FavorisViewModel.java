package com.dinfogarneau.coursmobile.exercices.tp3.ui.favoris;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;

import java.util.List;

public class FavorisViewModel extends AndroidViewModel {

    private LiveData<List<Propriete>> proprietes;
    private ProprieteRoomDatabase mdb;

    public FavorisViewModel(@NonNull Application application) {
        super(application);
        mdb = ProprieteRoomDatabase.getDatabase(application);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                proprietes = mdb.proprieteDAO().getAllLiveFavoris();
            }
        });
    }

    public LiveData<List<Propriete>> getFavoris() {
        return proprietes;
    }
}