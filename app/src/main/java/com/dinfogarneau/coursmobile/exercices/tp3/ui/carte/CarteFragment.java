package com.dinfogarneau.coursmobile.exercices.tp3.ui.carte;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.dinfogarneau.coursmobile.exercices.tp3.R;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CarteFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.InfoWindowAdapter, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener{

    private static final String TAG = "tag";
    private CarteViewModel carteViewModel;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;
    private Location userLocation;
    private Marker markerCamera;
    private GoogleMap mMap;
    private ProprieteRoomDatabase mDb;
    private List<Propriete> proprietes;
    private static final int LOCATION_PERMISSION_CODE = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        carteViewModel = new ViewModelProvider(this).get(CarteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity());

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    userLocation = location;
                    LatLng userPosition = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPosition, 12));
                }
            }
        };

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDb = ProprieteRoomDatabase.getDatabase(getActivity());

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                proprietes = mDb.proprieteDAO().getAllProprietes();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        for (Propriete propriete : proprietes) {
            LatLng position = new LatLng(Double.parseDouble(propriete.getLatitude()),
                    Double.parseDouble(propriete.getLongitude()));
            mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .title(propriete.getVille()))
                    .setTag(propriete);
        }

        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(this);

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);



        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                markerCamera.remove();
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Log.d(TAG, "onMapClick: " + latLng.toString());
                mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
            }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.d(TAG, "onMarkerDragStart: " + marker.getPosition());
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                Log.d(TAG, "onMarkerDrag: " + marker.getPosition());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.d(TAG, "onMarkerDragEnd: " + marker.getPosition());
            }
        });

        enableMyLocation();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(100000);
        locationRequest.setFastestInterval(50000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        if (ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(requireActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);

            return;
        }

        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.getMainLooper());

    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            ActivityCompat.requestPermissions(requireActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Autorisation accordée", Toast.LENGTH_LONG).show();
                enableMyLocation();
            } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext());
                    dialog.setTitle("Permission requise !");
                    dialog.setMessage("Cette permission est importante pour la géolocalisation...");
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(
                                    new String[]{
                                            Manifest.permission.ACCESS_FINE_LOCATION
                                    }, LOCATION_PERMISSION_CODE);
                        }
                    });
                    dialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(requireContext(), "Impossible de vous localiser", Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog.show();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        LatLng position = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
        markerCamera = mMap.addMarker(new MarkerOptions().position(position).title("Titre"));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        NavDirections action = CarteFragmentDirections.actionNavCarteToNavDetails((Propriete) marker.getTag());
        Navigation.findNavController(requireView()).navigate(action);

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.marker_layout, null);

        TextView tvType = view.findViewById(R.id.tv_marker_type);
        TextView tvPrix = view.findViewById(R.id.tv_marker_prix);
        TextView tvAddresse = view.findViewById(R.id.tv_marker_addresse);
        TextView tvVille = view.findViewById(R.id.tv_marker_ville);
        ImageView ivMarker = view.findViewById(R.id.iv_marker);

        Propriete propriete = (Propriete) marker.getTag();
        tvType.setText(propriete.getType());
        tvPrix.setText(propriete.getPrix());
        tvAddresse.setText(propriete.getAdresse());
        tvVille.setText(propriete.getVille());
        Picasso.get().load(propriete.getPetitePhoto())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder).into(ivMarker);

        this.getUserDistance(marker);

        return view;
    }

    public void getUserDistance(Marker marker) {
        Location location = new Location("Marker");
        location.setLatitude(marker.getPosition().latitude);
        location.setLongitude(marker.getPosition().longitude);

        Float distance = userLocation.distanceTo(location);
        Toast.makeText(requireContext(), "Disance: " + distance / 1000 + "km", Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(requireContext(), "onMyLocationButtonClick", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Log.d(TAG, "onMyLocationClick: " + location);
    }
}