package com.dinfogarneau.coursmobile.exercices.tp3;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.Menu;

import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.datahttp.VolleyUtils;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Commentaire;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;
import com.dinfogarneau.coursmobile.exercices.tp3.ui.details.DialogCommFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DialogCommFragment.EditCommentaireDialogListener {

    private ProprieteRoomDatabase mDb;
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDb = ProprieteRoomDatabase.getDatabase(this);
        refresh();


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_liste, R.id.nav_carte, R.id.nav_favoris)
                .setDrawerLayout(drawer)
                .build();


        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_supression:
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDAO().deleteAllCommentaire();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void refresh() {
        new VolleyUtils().getProprietes(this, new VolleyUtils.ListProprietesAsyncResponse() {
            @Override
            public void processFinished(ArrayList<Propriete> proprieteArrayList) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDAO().deleteAllPropriete();
                        for (Propriete propriete : proprieteArrayList) {
                            mDb.proprieteDAO().insert(propriete);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onFinishEditDialog(Commentaire commentaire) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.proprieteDAO().insert(commentaire);
            }
        });
    }
}