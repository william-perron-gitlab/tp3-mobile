package com.dinfogarneau.coursmobile.exercices.tp3.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "propriete")
public class Propriete implements Serializable {

    @PrimaryKey
    private int id;

    @NonNull
    @ColumnInfo(name="petite_photo")
    private String petitePhoto;

    @NonNull
    @ColumnInfo(name="grande_photo")
    private String grandePhoto;

    @NonNull
    @ColumnInfo(name="type")
    private String type;

    @NonNull
    @ColumnInfo(name="prix")
    private String prix;

    @NonNull
    @ColumnInfo(name="adresse")
    private String adresse;

    @NonNull
    @ColumnInfo(name="ville")
    private String ville;

    @NonNull
    @ColumnInfo(name="longitude")
    private String longitude;

    @NonNull
    @ColumnInfo(name="latitude")
    private String latitude;

    @NonNull
    @ColumnInfo(name="favoris")
    private boolean favoris;

    public Propriete(int id, @NonNull String petitePhoto, @NonNull String grandePhoto, @NonNull String type, @NonNull  String prix, @NonNull String adresse, @NonNull String longitude, @NonNull String latitude, @NonNull String ville, boolean favoris) {
        this.id = id;
        this.petitePhoto = petitePhoto;
        this.grandePhoto = grandePhoto;
        this.type = type;
        this.prix = prix;
        this.adresse = adresse;
        this.ville = ville;
        this.longitude = longitude;
        this.latitude = latitude;
        this.favoris = favoris;
    }

    public Propriete() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getPetitePhoto() {
        return petitePhoto;
    }

    public void setPetitePhoto(@NonNull String petitePhoto) {
        this.petitePhoto = petitePhoto;
    }

    @NonNull
    public String getGrandePhoto() {
        return grandePhoto;
    }

    public void setGrandePhoto(@NonNull String grandePhoto) {
        this.grandePhoto = grandePhoto;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    @NonNull
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(@NonNull String adresse) {
        this.adresse = adresse;
    }

    @NonNull
    public String getVille() {
        return ville;
    }

    public void setVille(@NonNull String ville) {
        this.ville = ville;
    }

    @NonNull
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(@NonNull String longitude) {
        this.longitude = longitude;
    }

    @NonNull
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(@NonNull String latitude) {
        this.latitude = latitude;
    }

    public boolean isFavoris() {
        return favoris;
    }

    public void setFavoris(boolean favoris) {
        this.favoris = favoris;
    }

}
