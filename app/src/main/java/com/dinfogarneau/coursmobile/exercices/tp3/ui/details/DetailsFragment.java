package com.dinfogarneau.coursmobile.exercices.tp3.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.dinfogarneau.coursmobile.exercices.tp3.R;
import com.dinfogarneau.coursmobile.exercices.tp3.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.tp3.data.ProprieteRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.tp3.model.Propriete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

public class DetailsFragment extends Fragment implements OnMapReadyCallback {

    private ProprieteRoomDatabase mDb;
    private DetailsViewModel detailsViewModel;
    private Propriete propriete;
    private String commentaire;

    private TextView tvType;
    private TextView tvPrix;
    private TextView tvId;
    private TextView tvStreet;
    private TextView tvCity;
    private TextView tvCommentaire;
    private FloatingActionButton btCommentaire;
    private ImageView ivDetails;
    private ImageView ivFavoris;

    private GoogleMap mMap;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        detailsViewModel = new ViewModelProvider(this).get(DetailsViewModel.class);
        propriete = DetailsFragmentArgs.fromBundle(getArguments()).getPropriete();
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fragmentManager
                .findFragmentById(R.id.map_details);
        if(mapFragment == null){
            // Create new Map instance if it doesn't exist
            mapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.map_details, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mDb = ProprieteRoomDatabase.getDatabase(getActivity());
        getCommentaire();

        tvType = view.findViewById(R.id.tv_details_type);
        tvPrix = view.findViewById(R.id.tv_details_prix);
        tvId = view.findViewById(R.id.tv_details_id);
        tvStreet = view.findViewById(R.id.tv_details_street);
        tvCity = view.findViewById(R.id.tv_details_city);
        tvCommentaire = view.findViewById(R.id.tv_details_commentaire);
        ivDetails = view.findViewById(R.id.iv_details);
        btCommentaire = view.findViewById(R.id.bt_commentaire);
        ivFavoris = view.findViewById(R.id.iv_favoris);
        Picasso.get().load(propriete.getGrandePhoto())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder).into(ivDetails);


        tvType.setText(propriete.getType());
        tvPrix.setText(propriete.getPrix());
        tvId.setText(Integer.toString(propriete.getId()));
        tvStreet.setText(propriete.getAdresse());
        tvCity.setText(propriete.getVille());

        if (propriete.isFavoris()) {
            ivFavoris.setColorFilter(R.color.yellow);
        }

        if (commentaire != null) {
            tvCommentaire.setText(commentaire);
        }

        ivFavoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                propriete.setFavoris(!propriete.isFavoris());
                if (propriete.isFavoris()) {
                    ivFavoris.setColorFilter(R.color.yellow);
                } else {
                    ivFavoris.clearColorFilter();
                }

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDAO().updatePropriete(propriete);
                    }
                });
            }
        });

        btCommentaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentaire != null) {
                    DialogCommFragment newFragment = new DialogCommFragment(commentaire, propriete.getId());
                    newFragment.show(getActivity().getSupportFragmentManager(), "Modifier commentaire");
                } else {
                    DialogCommFragment newFragment = new DialogCommFragment(propriete.getId());
                    newFragment.show(getActivity().getSupportFragmentManager(), "Ajouter commentaire");
                }
            }
        });
    }

    public void getCommentaire() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                commentaire = mDb.proprieteDAO().getCommentaireByPropId(propriete.getId());
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng position = new LatLng(Double.parseDouble(propriete.getLatitude()), Double.parseDouble(propriete.getLongitude()));
        mMap.addMarker(new MarkerOptions().position(position));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
    }
}
